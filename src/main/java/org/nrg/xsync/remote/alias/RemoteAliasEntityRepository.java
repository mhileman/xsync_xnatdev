package org.nrg.xsync.remote.alias;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Mohana Ramaratnam
 *
 */
@Repository

public class RemoteAliasEntityRepository extends AbstractHibernateDAO<RemoteAliasEntity>{

}
